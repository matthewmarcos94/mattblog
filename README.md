[![Netlify Status](https://api.netlify.com/api/v1/badges/40f05032-97a2-49e8-abf4-5c1fc17182f6/deploy-status)](https://app.netlify.com/sites/mattblog/deploys)
# Personal Website

Install gatsby-cli
Install netlify-cli

`npm install`

`npm run develop`

## To preview on a staging site:
`npm run preview`