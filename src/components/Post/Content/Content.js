// @flow strict
import React from 'react';
import styles from './Content.module.scss';
import { useSiteMetadata } from '../../../hooks';

type Props = {
  body: string,
  title: string,
  bookauthor?: string
};

const Content = ({ body, title, bookauthor }: Props) => { 
  // $FlowFixMe
  let { author }: { name: string } = useSiteMetadata(); // Too lazy to type this
  const hasAuthor: boolean = Boolean(bookauthor);

  return (
    <div className={styles['content']}>
      <h1 className={styles['content__title']}>
        {title}
        {/* $FlowFixMe */}
        {hasAuthor ? ` by ${bookauthor}` : ''}
      </h1>
      <div className={styles['content__body']} dangerouslySetInnerHTML={{ __html: body }} />
    </div>
  );
}

export default Content;
