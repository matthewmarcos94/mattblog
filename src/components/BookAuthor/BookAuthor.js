// @flow strict
import React from 'react';
import moment from 'moment';
import { Link } from 'gatsby';
import type { Edges } from '../../types';
import styles from '../Feed/Feed.module.scss';


const BookAuthor = ({ author }: { author: string }) => {
  if(!author || author === '') return null;
  return (<strong className={styles['feed__item-book-author']}>By: { author }</strong>);
};

export default BookAuthor;
