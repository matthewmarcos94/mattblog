---
slug: '/posts/the-benefits-of-working-from-home'
date: '2020-01-16T00:00:00.001Z'
title: 'The Benefits of Working From Home'
template: "post"
draft: false
# bookauthor: 'Massimo Pigliucci'
category: "Blog Posts"
description: "Seriously consider remote work if you want to live a meaningful life."
tags:
  - "Productivity"
  - "Pomodoro"
  - "Remote Work"
---
![Workin](/media/wfh1.jpeg)

Even after having worked from home as a Software Engineer for a little over a year now, and I am still discovering more benefits from this arrangement. Although some of them may seem trivial and gimmicky, it would be foolish to brush off working remotely if you want to live a happy and meaningful life. I have been very grateful for this kind of arrangement at work until now.

## You get more time to sleep
The first advantage I had noticed from working from home full-time was the freedom to get a full-night’s sleep every day even when I had to work extra hours.

The importance of having enough sleep is underappreciated by young professionals because those who sleep early get stigmatized as weak or lazy. However, as you grow older and develop higher expectations from yourself, squandering sleep starts to feel wasteful when you walk in the office fatigued and ineffective.

Consistently sleeping well provides tremendous benefits that become harder to ignore, such as increased energy for daily activities and an upswing in wellness. People with better sleep experience an improvement in their decision making, and information absorption. This is an essential boon for people serious about being on top of their tasks and having an edge over competition.

On the other hand, constant sleep deprivation has harmful side-effects that can seriously dampen your chances at success. It messes with your ability to focus, messes with your hormones (that can have consequences worse than rapid weight gain), and ages you faster.

My acquired sleeping habits (that have been made possible only due to my work arrangement) have started to pay dividends in my achievements and my health.

## You cook your own food
Working from home has allowed me to prepare my own food. It is generally accepted that home-cooked food is far more healthful than what you would order outside. You control everything about what you eat - from the ingredients, the manner of preparation, and the portion sizes. Restaurant food prep is a blackbox. Unless you know the chef, there is zero visibility in how they make your food.

<figure class="float-right" style="width: 240px">
	<img src="/media/wfh2.jpeg" alt="Crockpot">
	<figcaption>Cooking healthy is so easy. You just cut your ingredients and dump them into the crockpot!</figcaption>
</figure>

Restaurants have to make money on top of their overhead liabilities, and unfortunately, your bank account will take the brunt of that responsibility. Learn to cook at home and you can save (in my experience) more than half of what you would have spent eating outside.

The only “reasonable” argument I hear against home cooking is the extra time expenditure acquiring recipes, cooking, and washing the dishes. However, if you meal-plan properly and use a crockpot (for added effectiveness,) most of your this time expenditure can be front-loaded in the weekend. I daresay that reheating your meals everyday even saves you more time in the long run!

## Your quality of life improves
Any Manileno can attest to the egregious traffic conditions in the city. It is possible to waste half a day and most of your energy on the commute before and after work. Working remote was an improvement from my previous job where I had to choose between spending more than nine hours on the road, spending more than a day’s salary on a Grab ride, or sleeping on the office floor at night.

The removal of my daily commute has obviated the need to purchase face masks and top up on Grab credits in the (common) occasion that the bus or the train breaks down. I can avoid the long queues that worsten during the rainy season. Because I am not exposed to the airborne pathogens that contaminate public spaces, I spend less time feeling unwell.

The additional time and energy that has been added to my disposal has allowed me to take on more challenges in work and in my personal life. There is less compulsion to stop working so early lest I get stuck in those six hour traffic jams. Work has been so much more immersive now that it has been easier for me to implement the changes that I wanted to see.

Over the last year, I was able to maintain and even pick up new hobbies that would have never been possible if the commute consumes most of my time. My free time was spent reading about everything that piqued my interest, learning to cook (for real), practicing carpentry, and picking up some weights. None of these required too much monetary spending.

## What are the downsides of working from home?
The biggest downside of doing remote work is the difficulty separating work from personal life. There is a level of discipline required to squelch the pesky demons coaxing you to “Netflix and bill”. Personally, I had to develop some [mechanisms](/posts/why-use-the-pomodoro-technique) to keep myself focused and productive even on slower days. I still wear office clothes when I work, and I maintain a routine that allows me to effectively accomplish my daily tasks. It wasn’t easy, considering the proclivities that carried over from my collegiate days, but I could still say that I have only become more resourceful and gritty.

When choosing a job, you are choosing a lifestyle. It does not make sense for me to work in the city with all the soul-siphoning overhead, when it would cost me my savings, health, and time. By reclaiming that time outside of work, I would say that I have developed a multifaceted personality and explored my interests.
