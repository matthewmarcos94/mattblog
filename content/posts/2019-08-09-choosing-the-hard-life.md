---
slug: '/posts/choosing-the-hard-life'
date: '2019-08-09T00:00:00.001Z'
title: 'Choosing the Hard Life'
draft: false
template: "post"
category: "Blog Posts"
description: "How do we continuously improve?"
---
My improvements in the last few months have been the results of choosing to do the difficult things when I had the choice not to.

It was choosing to go to the gym despite feeling lazy and unmotivated.

It was choosing to eat chicken and broccoli despite having the option to snack on the pizza right next to it.

It was choosing to get up at 5 am every day despite feeling tired and sleepy.

It was choosing to watch Coursera despite having the option to watch Youtube and other tension-relievers.

It was choosing to start that Pomodoro timer at work despite feeling the need to watch Youtube or sleep-in.

Choosing the harder path has developed a strength in myself that I can invoke whenever I need the motivation and the willpower to go through another hard phase in my life.

I once read *Outliers* by *Malcolm Gladwell*, and I can see the phenomenon of *The Law of Accumulated Advantage* unfold for me. The more I win, the easier it becomes for me to make more wins. The more books I read, it becomes easier for me to finish books. The more weight I lift, I tend to pull even more weight the following week. The more I discipline myself to make better choices, and it becomes easier for me to make better choices. By putting myself in an advantageous position each day, I am unlocking more pathways to success.

The human body is amazing because it can adapt to your circumstances in life. Once you start applying the same discipline to your mental health and growth, you will learn that the mind has so much more power than the body.

In the span of only a few months, I have turned from a self-loathing, depressed, and bitter slob into a more positive, confident, and grateful person.

If you genuinely want to improve, then you have to make the decisions that will benefit you in the long term despite the short-term pain it might cause.

You improve your body with proper nutrition, sleep, and exercise. You strengthen your mind by reading, writing, and practicing self-discipline.
The more frequently you do this, you are building momentum that will propel you towards your goal. Before long, you will be unstoppable.

If you do not make the hard choices starting now and things to south, there won't be anything for you to look back on that will motivate you to go further.

You have been well acquainted with Murphy. Things will always go wrong.