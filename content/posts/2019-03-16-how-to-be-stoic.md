---
slug: '/book-notes/how-to-be-stoic'
date: '2019-06-29T00:00:00.001Z'
title: 'How to be a Stoic'
draft: false
bookauthor: 'Massimo Pigliucci'
template: "post"
category: "Book Notes"
description: "How do we find the strength to maintain our identities and character amidst all the chaos in this world?"
tags:
  - "Massimo Pigliucci"
---
![How to be a Stoic cover](/media/how-to-be-stoic.jpg)

How to be Stoic by Massimo Pigliucci introduces us to the school of philosophy known as Stoicism. This book can provide its readers the mindset and outlook needed to properly contend with the intense and unceasing challenges we are subject to in the modern world. The ideas in this book, when combined with further reading and consistent, deliberate practice, will drastically improve the practitioner’s life from being one plagued with suffering and stagnation to one brimming with focus, achievement and freedom.

## What is Philosophy?
A philosophy is a way of thinking that guides each individual how to live life. By defining which pursuits are to be aimed for and the manner the are to be effected, philosophy becomes the underlying counsel that influences how an individual makes their daily choices. It tries to explain the universe, and how its constituents should behave.

## What is Stoicism?
Stoicism is one of the many philosophies that have been developed throughout history. It originated in Ancient Greece on the humble Stoa, or porch, of Zeno where he began to teach his philosophy to a small following. In the ensuing centuries, many profound and respectable characters like Franklin Roosevelt and have turned to Stoicism and utilized its ideas to overcome the challenges that had befallen the world during their times. By learning about Stoicism, we are enabling ourselves to harness the full might of proven, applicable, and timeless principles to not only contend with the vicissitudes of life, but to make the world more tolerable for its inhabitants.

There are two tenets central in Stoicism. The first is the dichotomy of control, which is a way to categorize daily occurrences as either within your control or not. Practitioners of Stoicism or Stoics believe that it is prudent that an individual should not let the things that are not in their realm of control cause anxiety, hindering rational thinking. Such things have to be let go. Instead, one should focus on the things that they can influence.

I have found ways to apply this mindset every single day. The roads of the Philippines are perpetually clogged and going as far as 10km might take over an hour. Some people might argue that it is more time efficient to walk, but keep in mind that it rains in this country half the year. For the remaining half of the year, the sweltering heat will pierce through your skin and singe the hairs at the back of your neck. It’s better to wait in the relative comfort within the sardine-can-like formation that daily commiters take to cram themselves inside public transport.

It is absolutely easy to be absolutely vexed at the world and everyone in it when you are forced to hold this formation for nine hours everyday to get to work, however, by accepting what I could not change, I was able to focus on other productive things like listening to podcasts. At least it kept me sane until I was able to find the opportunity to avoid the daily commute.

By using rational thinking, you are using your full potential and bringing your whole being into solving whatever problems you’re encountering.

The dichotomy of control can help us a lot on our daily activities. When we are stuck in traffic, we can either complain about it or

The second tenet of Stoicism is to follow the four cardinal virtues. A virtue is an ideal that must be embodied under any circumstance. Acting out these virtues is the single most important thing you should strive for, since nothing else is of equal value. Stoics aim to to achieve eudaiomonia, which according to Aristotle, is the ethically good life - the perfect state of living - by embodying the four virtues all the time.

The first cardinal virtue is **phronesis**, often translated as practical wisdom. Phronesis describes the ability to use the rational judgment when choosing which actions must be taken. According to Socrates, wisdom is the chief good because it is the only human ability that is good under every circumstance. Phronesis held so much importance in Ancient Greece such that some philosophers think that no single word today can carry its meaning. Phronesis allows allows an individual to make decisions that will benefit themselves and others.

The second of these four virtues is **courage**, which is the capacity of an individual to turn around and face life for what it really is, and do what is morally correct despite the short-term consequences. It is the ability to do what is right in difficult conditions. Courage as a virtue is the same mechanism that drives people to do heroic things despite the possibility of painful consequences. These include bravery, perseverance and authenticity

The third one is **justice**, which is to treat every human with dignity, fairness and kindness regardless of their stature in life. Nobody truly intends to do evil. When people do things that are harmful and detrimental to society, then it is their judgment that is impaired, and should therefore be treated as if they have a disability. Leadership and teamwork are all parts of justice

The fourth virtue is **temperance**, which is to reject indulgence and exercise self-control in all aspects of life. This helps people avoid excess. The Romans lived in excess. They had orgies, and gladiator fights for entertainment

If there are virtues, then there are vices. Vices are actions that are always bad for every party involved. Vices must be avoided under any circumstance no matter how tempting they are.

Anything that does not categorize as a virtue or a vice is an indifferent. An indifferent can be preferred, like money. It can be dispreferred like pain

Stoics practice a humanity called oikeiôsis, which is quite similar to the Cynic-Stoic concept called Cosmopolitanism. This is the concept where are at the center of various concentric circles of the communities we live in. We must extend our kindness to ourself, then our kin, then our country, then the world.

Stoics also practice living their worst fears. For us, it could include going for a week without shopping. This gives is the perspective of what it really feels like to live out our worst fears so that we are prepared when it happens. It helps Stoics stay grounded.

I can apply Stoicism by reading the lessons of the ancient Stoic Philosophers like Epictetus, Seneca, and Marcus Aurelius. I have recently installed an application that guides me in my meditation every morning and guides me through my self-reflection in the evening. I can do that.


## Notes
### Stoic Principles
1. Virtue is the highest good and everything else is indifferent. Nothing is to be traded against virtue.
1. Follow Nature. Use reason but do not forget that we are still subject to higher forms of power.
1. Dichotomy of control.

### 12 Exercises
1. Examine your impressions
1. Remind yourself of the impermanence of things
1. The reserve clause
1. How can I use virtue here and now?
1. Pause and take a deep breath
1. Other-ize
1. Speak little and well
1. Choose your company well. Also be selective with your attention.
1. Respond to insults with humor
1. Don’t speak too much about yourself
1. Speak without judging
1. Reflect on your day
