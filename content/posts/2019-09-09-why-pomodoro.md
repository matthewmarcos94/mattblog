---
slug: '/posts/why-use-the-pomodoro-technique'
date: '2019-09-09T00:00:00.001Z'
title: 'Why Use the Pomodoro Technique'
template: "post"
draft: false
category: "Blog Posts"
description: "How can we be sustainably productive for long periods of time? Can we take on even more responsibilities while reducing stress?"
socialImage: "/media/pomodoro.jpg"
tags:
  - "Pomodoro"
  - "Productivity"
  - "Discipline"
---
![Pomodoros](/media/pomodoro.jpg)

The saying *"measure what you want to improve"* is not just a cheap cliche. If you are serious about intensifying your work productivity, then you must associate a metric to it. In doing so, you are achieving a couple of things: First, you can use it as a barometer to assess how well you have been doing at any particular moment. Secondly, you can set a measurable goal that can define failure if you do not get your act together. From a psychological standpoint, consistently seeing that metric multiple times throughout your workday will remind you to be responsible for your commitments.

The impact you produce in a given period is what measures productivity. The two things that can affect this are the tasks you elect to do, and the amount of time you choose to work on them. The Pomodoro technique aims to improve the quality and quantity of time you put into your tasks. The tasks you opt into are entirely up to you. You can use other techniques like the **Eisenhower Box** to prioritize which duties to perform first. The Pomodoro technique will not guarantee improved productivity if combined with poor prioritization. However, it will *generally* increase your impact, *ceteris paribus*.

### How to Pomodoro (at least from a 1,000-foot view)
I will not go down the rabbit hole of the intricacies of the Pomodoro technique. Francesco Cirillo's book (the inventor if this technique) already provides reasonably detailed instructions on handling the various forms of distractions that could arise. The general flow is as follows:

1. Make a list of tasks you want to accomplish throughout the day.
2. Estimate the number of pomodoros it takes to complete a task. A Pomodoro is an indivisible unit of work that consists of 25 minutes of undivided attention towards that task.
3. Set a timer for each Pomodoro and start working until the timer rings. Do not do anything unrelated to work - including bathroom or washroom breaks.
4. If you get distracted, write the distraction down and continue working. If you must stop, cancel the Pomodoro and start again later.
5. Between every Pomodoro, take a 5-minute break. Every four Pomodoros take a 20-minute break.

### How I started using Pomodoro
The work formula I had after graduating from University included haphazardly jumping between my weekly tickets and browsing social media. This habit resulted in frequent late-night work and general stress. Myself being a software engineer, my job required deep thinking and concentration. Jumping around between different contexts and chatting with my friends in between only fragmented the attention I needed.

The first time I heard of the Pomodoro Technique was from my engineering lead at my first job. She adviced me to try using it so I can be more productive throughout the day. Being a young and idealistic (and honestly stupid) kid, I ignored her advice. I did not actually try using it until I was into my second job.

My next job was remote. I quickly realized that only I was responsible for my output. By this point, I had matured a little more and had started expecting greater things from myself. My first goal was to be the most productive I can be every single day. That was the time I realized that the Pomodoro technique is the perfect tool to solve my problem.

### What exactly improved?
The no-distraction rule allowed me to get things done quickly. The timer also prevented me from procrastinating at the start of the day. Since I was logging the amount of work I had been putting into my tasks, it became easier to justify why some things have sometimes taken longer than estimated, indicating an error in project management than work-ethic.

Consequently, my work stress almost disappeared because I knew that I was doing honest and intense work at my maximum capacity.

The five-minute break between Pomodoros allowed me to take a step back and think whether or not my solutions were optimal. It served as the perfect reminder to check my slack messages, which I often forget to do in my previous longer swaths of coding time. It also prevented me from hitting the wall too early.

As time went on, disciplining myself became easier. I could consistently push quality code in a shorter timeframe. This capability ultimately left me free by the afternoons, having finished my critical tasks before lunch. With more free time at my hands, I could do more meaningful, non-work related things like reading, working out, and creating side-projects.

### Measuring Productivity
I log my pomodoros with a notebook in conjunction with an app called *productivity timer*. This app gives you a rank based on how well you did the previous seven days. Think of it as a rolling sum graph with a 7-day window. It's perfect because you have to be consistent with your logging and work habits.

It did not take me long to realize that my Pomodoro count usually drops past noon after taking a lengthy, hour-long lunch break. I learned the importance of momentum and how it affects me and my attitude at work.

### Iterating and improving
I make continuous adjustments and experiments based on my weekly metrics. Some of these changes are:
1. I shifted my workday to start between 5, and 6 am so I can hit most of my Pomodoro goal before the lunch break kills my momentum.
1. I have extended my Pomodoro to 30 minutes and taking the long break every five intervals instead of four.  This change has made work feel a little more natural.
1. On terrible days, I allow myself to cheat a little and revert to the 25-minute Pomodoro with long breaks every four intervals.

I allocate at least 14 Pomodoros to my tasks at the start each day - that's 7 hours of real work. I average at around 10-12 because:
1. Our best friend Murphy sporadically throws shit at us.
1. I underestimate my coding speed when I'm balls deep.
1. I never count meetings (which take over a couple of hours if they include client demonstrations and intense discussions!).
1. The hot and humid Phils weather (yes, I sometimes blame the weather) might cause us to visit the pantry frequently.

I think that hitting 10 is a good average. Do not beat yourself for doing significantly fewer, especially when you're not used to this level of self-discipline.