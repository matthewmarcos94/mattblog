lsq---
slug: '/book-notes/the-effective-engineer'
date: '2019-11-01T00:00:00.001Z'
title: 'The Effective Engineer'
bookauthor: 'Edmond Lau'
template: "post"
category: "Book Notes"
description: "My book notes on The Effective Engineer by Edmond Lau"
tags:
  - "Edmond Lau"
  - "Book Notes"
---
<figure class="center" style="width: 270px">
	<img src="/media/the-effective-engineer.jpg" alt="The Effective Engineer">
	<!-- <figcaption>Johannes Gutenberg</figcaption> -->
</figure>

I want to disclaim that some of the statements in these notes are copied verbatim from Edmond’s book. I chose to do this because this “publication” is a compilation of my notes, my thoughts, and some of the ideas I have found interesting. I only use this to frequently remind myself of the values I learned in this book.

I think that reading The Effective Engineer by Edmond Lau is necessary for all software engineers and product managers if they want to improve their value for their organizations. 

In this book, Edmond draws on his engineering experience in Google and subsequent experience in growing startups to tell stories of the common problems that engineers face in the workplace and how to properly get around them. 

The Effective Engineer is an easy read, similar to most self-help books. It contains philosophies that can be adapted to ensure better decision making throughout one’s workday. Unlike the majority of software engineering textbooks, there is scant technological jargon that will require you to google every five minutes. I opine that it reads like a novel and can be placed on your bedside table for light consumption after a long day.

## Table of Contents
```toc
# This code block gets replaced with the TOC
exclude: Table of Contents
tight: true,
from-heading: 1
to-heading: 6
```

# Adopt the Right Mindset
## Chapter 1: Focus on High-Leverage Activities
In this chapter, Edmond describes leverage. Leverage, as he defines it, is the ratio of the impact an engineer produces to the time spent creating it. As an engineer, you want to focus on activities that produce the most value for time because time is your most limited resource. There will come a point that there is more to be done than time available, so you will have to select the best tasks for your time.

$$
Leverage = Impact / Time
$$

This sounds like a pretty obvious statement, but one of the cases that Edmond tries to make in this chapter is that some high-leverage tasks do not make you feel inherently productive. Consider the onboarding process. Many engineers dislike having to be responsible for other employees at this stage. It might be boring, repetitive, and worst of all, it distracts you from doing what you were originally hired to do - code!

Defining high-leverage tasks is not as easy as it seems. Just like in Economics, there are multiple variables and side effects to consider when selecting which actions are most suited for your time.

The way I view high-leverage activities is that they pay off like and investment with compound interest. It is better to start them off as early as possible so you can get higher returns over time. 

Here are some of the most impressive things that I have been made aware of:
1. Mentoring a new hire for 1 hour every day for their first month (20 hours) might seem like a costly investment but it only represents 1% of the time they spend coding in their first year. 
1. Spending 20 hours of your time mentoring other engineers can potentially save them hundreds of hours in the long run. 
1. Sometimes, by taking a couple of hours to teach your mentees new commands or tools, you can save them hundreds of hours. 
1. Doing code reviews early on to catch bad practices before they become habits can save hundreds of engineering hours in the future by avoiding unnecessary bug fixes and avoidable refactoring.
1. Teaching new hires, especially new graduates how to prioritize tasks can help them increase their productivity

### How to Increase Leverage
There are three ways to increase leverage:
1. Reduce the time it takes to complete a certain activity.
1. Increasing the output of a certain activity.
1. Shifting to higher-level activities.
We can determine how we can improve by asking these three questions: 
1. How can I finish this activity faster?
1. How do I make this activity more valuable?
1. Is there something better that I can work on?

### Increasing Leverage in a Meeting
Edmond’s first example is how we can increase the leverage of a one-hour meeting:
1. Cut the time to (to half an hour) so you discuss only the important points.
1. Prepare an agenda beforehand.
1. Chat asynchronously via email if an in-person meeting is unnecessary.

At the time of this writing, I have a full-time, remote software engineering job at a firm based in a country two timezones away. This has made meetings difficult to schedule since engineers work at different times. 

By replacing most meetings with chatting via slack and sending screen recordings for demonstrations, engineers are free to do things at their own pace without taking the other’s time. Directives are easier to log and summarize because everything gets written down. Timestamps are useful for ensuring accountability. I also feel that it has helped me develop my communication skills by forcing me to separate the wheat from the chaff to keep messages short and succinct.

### Increasing Leverage as a Product Engineer
The second example in leverage is a product engineer creating a new feature. He could increase leverage by:
1. Automating repetitive things. This is what DevOps culture is all about.
1. Prioritizing critical tasks.
1. Get feedback from stakeholders to understand their biggest problems.

### Increasing Leverage as a Performance Engineer
The third example is a performance engineer trying to locate and expedite system bottlenecks.
1. Study your profiling tools so you can reduce the time to discover a new bottleneck.
1. Identify which bottlenecks are affecting the most traffic so you can prioritize them.
1. Prevention is the best cure. Work with product teams so that performance is considered a feature rather than a bug. This reminds me of DevOps culture wherein reliability and becomes a feature of the dev team while having frequent releases gets included in the ops team’s responsibilities.

### The Hiring Process is a High Leverage Activity
An engineering director at Facebook improved the company’s engineering team’s strength by encouraging other engineers to partake in the hiring process. At first, engineers were reluctant to depart from their traditional roles. However, once this director convinced the other engineers that doing interviews improved their hires, consequently improving the team, the engineers stopped skipping interviews.

This same director also encouraged recruiters to schedule potential hires in the “first humanly-possible time slot”. This allowed Facebook to close offers with excellent talent before their competitors.

## Chapter 2: Optimize for Learning
Do not be afraid of discomfort and challenges. The short-term pain from doing something novel will benefit you in the long run - so much so that it may even be worth seeking it.

### Adopt a Growth Mindset
**Fixed Mindset** - Believing that the intelligence and skills you are born with are fixed. You cannot improve them any further.
**Growth Mindset** - To view people as beings capable of development. Having a growth mindset means believing that skills can be improved with study and practice.

Always adopt a growth mindset even though the path to your goals are not always straightforward.

### Invest in your rate of Learning
Learning follows an exponential growth rate. Learning gives you knowledge, and the more knowledge you have, the easier it becomes to learn.

Consider the following ideas:
1. Learning recursion is the basis for many concepts like trees and graph searches, which in turn are essential to understanding networks.
1. Having a good job makes it easier to get a better second job, which will affect your career.
1. Even small changes in your learning rate are good because of the compound effect of learning.

To avoid stagnation, treat yourself like a startup. Startups are willing to defer profitability to increase their chance for success. There is a concept called LEAN where startups release a minimum viable product first but constantly improve on it. Likewise, you should treat yourself as a work-in-progress that needs to be improved daily. Always put yourself in a situation where you can learn new things.

### Seek Work Environments Conducive to Learning
A work environment that promote learning may have the following characteristics:

**Fast growth** - Companies whose problems exceed its resources provide its employees many opportunities to go on dragon-slaying quests and provide a disproportionately large impact. Slower-moving companies, however, may provide limited opportunities for people to grow. Some questions you may ask:
1. What is the growth rate of their core business metrics?
1. Are you going to be working on high-priority tasks with adequate company resources?
1. How aggressive has the company been hiring over the past few months?
1. Does the career path of engineers who have worked that position pique your interest?

**Training** - How is the training of that company? Do they have good onboarding programs? Will there be mentorship? How many resources are they willing to invest in your learning?
1. Is each new person expected to learn things on their own, or is there a more formalized way of onboarding engineers?
1. Does any form of mentorship take place?
1. What has the company done recently to develop their engineers’ skills?
1. What new things have the team members learned recently?

**Openness** - You cannot expect an organization to know everything immediately, but they should be able to learn from their past mistakes and figure things out. That is more likely to happen if the employees are curious and open with feedback.
1. Do employees know what priorities different teams are working on?
1. Do teams review the effectiveness of their decisions after launch?
1. Do teams do postmortems?
1. How is knowledge documented and shared?
1. What are some lessons the teams have learned?

**Pace** - A company that iterates quickly gets feedback faster, thus allowing their engineers to learn at a faster rate. Ideally, you want to work in a company that has fewer bureaucratic barriers. This means shorter and simpler internal processes and fewer single points of failure for approvals.
1. Is a fast-paced environment reflected in the company culture?
1. What tools does the company use to improve iteration speed?
1. How long does it take between an idea’s conception to launch approval?
1. How much time is spent on maintenance versus creating new products and features?

**People** - Ideally, you want to surround yourself with people who are smarter and more talented. This also means surrounding yourself with potential teachers and mentors. Ensure you are able to choose which people you will be working with. 
1. Do the people who interviewed you seem smarter than you?
1. What skills can they teach you?
1. How good was your interview? Do you think that you can gel with the kinds of people who can pass it?
1. Do people work in one-man teams or is there cooperation?

**Autonomy** - You want to join a company that gives you the freedom to choose the way you will work. Of course, this will mean that your responsibilities will increase (which is absolutely great!).
1. Do engineers have the option to choose which projects to work on and how they do them?
1. How often do engineers switch teams or projects?
1. How much of the codebase will an individual be able to work on over the course of a year?
1. Do engineers participate in discussions on product design? Do their voices have weight in such meetings?
In general, onboarding and mentoring are important earlier in your career while autonomy matters later on. Make sure you are asking the right questions when looking for a job.

### Dedicate Time on the Job to Develop New Skills

Do not fall into the trap of trying to catch up that you neglect to develop the skills that will help you work more effectively. Instead, try taking a chunk of time from your daily web-browsing to actually work on something that will benefit you in the long term. These could be adjacent disciplines or tools you already use. Try to do the following things in that time:
1. Study code written by better engineers.
1. Write even more code if it isn't part of your daily responsibilities.
1. Go through internal technical and educational materials available.
1. Master the programming languages you use.
1. Send your code reviews to the harshest critics.
1. Enroll in classes where you want to improve.
1. Participate in design discussions of projects you’re interested in.
1. Work on a diversity of projects..
1. Make sure you’re working on a team with a more senior engineer you can learn from.
1. Code fearlessly. Engineering success has a positive correlation with “having no fear when jumping into code they don’t know”.

### Always be Learning
