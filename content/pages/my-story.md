---
title: "My Story"
template: "page"
socialImage: "/media/image-2.jpg"
slug: '/my-story'
---

I am a software engineer with a peculiar fascination for solving complex puzzles, especially those presented in those Mensa problem sets.


I try to avoid unidimensionality by completing a variety of activities every day. I devour books like a five-year-old goes through candy, finishing between two and six each month. I enjoy trying new recipes, almost as frequently as new JavaScript frameworks come out. I love working out at the gym and noodling with my guitars. Look at them!

<figure class="center">
	<img src="/media/elk-de-guitarra.jpg" alt="Still have to study photography though" title="Still have to study photography though">
	<figcaption>TODO: String my classicals and take a better pic</figcaption>
</figure>


This year, I decided to distill and publish all the **essential** knowledge that I acquire from worthy books and well-written essays. They are meant to serve as personal references when I'm in a pinch. If you ignore the idiosyncrasies of my writing, perhaps they will be useful to you as well.

I firmly believe that success (with the $ as the metric) is a result of conscientiousness, competency, and ruthless action. I write about these in my blog as well.
