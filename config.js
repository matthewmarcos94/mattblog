'use strict';

module.exports = {
  url: 'https://lumen.netlify.com',
  pathPrefix: '/',
  title: 'Matthew Marcos',
  name: 'Matthew Marcos',
  subtitle: 'Chasing Challenges',
  copyright: '© All rights reserved.',
  disqusShortname: '',
  postsPerPage: 4,
  googleAnalyticsId: 'UA-136830154-1',
  useKatex: false,
  menu: [
    {
      label: 'Home',
      path: '/'
    },
    {
      label: 'Blog Posts',
      path: '/category/blog-posts'
    },
    {
      label: 'Book Notes',
      path: '/category/book-notes'
    },
    {
      label: 'My Story',
      path: '/my-story'
    }
  ],
  author: {
    name: 'Matthew Marcos',
    photo: '/photo.jpg',
    bio: 'Software Engineer @ Station Five',
    contacts: {
      email: '',
      facebook: '',
      telegram: '',
      twitter: 'matthewmarcozz',
      github: 'matthewmarcos94',
      rss: '',
      vkontakte: '',
      linkedin: 'matthewmarcos94',
      instagram: '',
      line: '',
      gitlab: '',
      weibo: ''
    }
  }
};